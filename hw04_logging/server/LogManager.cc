//
// LogManager.cc
// DBhw04
// Wooin Lee
// 2014-22572
//
#include "LogManager.h"
#include <iostream>
using namespace std;

LogManager::LogManager(){
	logfile = fopen("server.log", "a");	
	isStop = false;
	flushAll = false;
	mLSN = 0; // log sequence increasing
	leftOver = 0;
}
LogManager::~LogManager(){
	hardFlush();
	isStop = true;
	fclose(logfile);
}

void LogManager::hardFlush(){
	flushAll = true;
}

void LogManager::runFlushThread(){
	int LSN;
	int count = 0;
	int flush_frequency = 50000;	// check buffer every 50000 cpu time
	clock_t last_check = clock();
	while(true){
		if(isStop) break;
		
		if(clock()-last_check > flush_frequency){
			leftOver = log_buffer->size();
			last_check = clock();
		}

		if(flushAll || (log_buffer->size() > 0 && leftOver == log_buffer->size())){
			Flush();
		}
	}
}
void LogManager::Flush(){
	int buffer_size = log_buffer->size();
	int LSN;
	//cout << "Flushing : "<< buffer_size <<endl;
	for(int i = 0; i < buffer_size; i++){
		unique_lock<mutex> lock(log_mutex);
		LSN = log_buffer->dequeue();
		fprintf(logfile, "%d|%s\n", LSN, log_records[LSN].c_str());
		fflush(logfile);
		log_records.erase(LSN);
	}
	leftOver = log_buffer->size();
}

void LogManager::addLogBuffer(string log){
	// flush if the log buffer becomes full
	if(log_buffer->isFull()){
		Flush();
	}
	unique_lock<mutex> lock(log_mutex);
	log_buffer->enqueue(mLSN);
	log_records[mLSN++] = log;
}


