// 
// Util.h
// DBhw04
// Wooin Lee
// 2014-22572
//
// Auxiliary functions for the DB
#include "Util.h"

bool isDouble(string & str)
{
	if (str.find(".") != string::npos) {
		return true;
	}
	else{
		return false;
	}
}

int getType(string & str){
	if (isDouble(str))
		return 1;			 // double is denoted 1
	else
		return 0;			 // integer is denoted 0
}

string splitString(string& str){
	string sub;
	const string delimiter = "|";
	short int token_length = 1;
	size_t pos = str.find(delimiter);

	if (pos == string::npos){
		sub = str;
		str = "";
		return sub;
	}
	sub = str.substr(0, pos);   // split first token found
	str.erase(0, pos + token_length); // erase the token from the original string
	return sub;
}
