//
//  RowTable.cpp
//  DBhw04
//  Wooin Lee
//  2014-22572
//
#include "RowTable.h"

using namespace std;

RowTable::RowTable(FILE* ofs_){
    mNumRows = 0;
	ofs = ofs_;
}

RowTable::RowTable(Scheme scheme_, FILE* ofs_){
    // init scheme
    mScheme.field_count = scheme_.field_count;
    mScheme.field_type = (int*)malloc(sizeof(int)*scheme_.field_count);
	mScheme.field_offset = (int*)malloc(sizeof(int)*scheme_.field_count);
	int sum_offset = 0;
	for (int i = 0; i < mScheme.field_count; i++){
		mScheme.field_type[i] = scheme_.field_type[i];
		mScheme.field_offset[i] = sum_offset;
		sum_offset += (mScheme.field_type[i] > 0) ? sizeof(double) : sizeof(int);
	}
    mScheme.recordSize = scheme_.recordSize;
    
    // init first table page
    pageBuffer = (TablePage*)malloc(sizeof(TablePage));
    pageBuffer->records = (char*)malloc(mScheme.recordSize * PAGE_SIZE);
    
    // add empty page to the table for later access
    mTable.push_back(pageBuffer);
	ofs = ofs_;
}

RowTable::~RowTable(){
    // clean up codes - return allocated memory
    for(int i = 0; i < mTable.size(); i++){
    	free(mTable[i]->records);
    	free(mTable[i]);
	}
    free(mScheme.field_type);
	free(mScheme.field_offset);
}

void RowTable::load(string path){
    ifstream ifs(path);
    
    if (!ifs.good()){
        cout << "File open error!" << endl;
        exit(1);
    }
    bool isFirst = true;
    string val, line, line_backup;
    
    while(getline(ifs, line, '\n')){    // tokenize each string to table row struct
        if(isFirst){                    // create data scheme and a row buffer
            isFirst = false;
            line_backup = line;
            readScheme(line);
            // create an initial page
            
            pageBuffer = (TablePage*)malloc(sizeof(TablePage));
            pageBuffer->records = (char*)malloc(mScheme.recordSize * PAGE_SIZE);
            line = line_backup;
        }
        /**************************************
         When page is full, start a new page  */
        if(mNumRows == PAGE_SIZE){
            pageBuffer = (TablePage*)malloc(sizeof(TablePage));
            pageBuffer->records = (char*)malloc(mScheme.recordSize * PAGE_SIZE);
            mTable.push_back(pageBuffer);
            mNumRows = 0;
        }
        /********************************************************************
         Record format in the TablePage                                     *
         [column0][column1][column2]...                                     */ 
		//write the record to the buffer page
		char* baseAddrs;
		for (int i = 0; i < mScheme.field_count; i++){
			baseAddrs = &mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize + mScheme.field_offset[i]];
			// write column
			val = splitString(line);
			if (mScheme.field_type[i] > 0){
				double dField = atof(val.c_str());
				memcpy(baseAddrs, &dField, sizeof(double));
			}
			else{
				int iField = atoi(val.c_str());
				memcpy(baseAddrs, &iField, sizeof(int));
			}
		}
        mNumRows++;
    }
    ifs.close();
}

void RowTable::insertRecord(char* record){
    // if last page is full, build a new page
    if(mNumRows == PAGE_SIZE){
        pageBuffer = (TablePage*)malloc(sizeof(TablePage));
        pageBuffer->records = (char*)malloc(mScheme.recordSize * PAGE_SIZE);
        mTable.push_back(pageBuffer);
        mNumRows = 0;
    }
    /********************************************************************
     * Data format in the TablePage                                     *
     * [pointer0][pointer1][pointer2]... [column0][column1][column2]... */
    
     //Target address ::: mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize];
	char* baseAddrs;
	for (int i = 0; i < mScheme.field_count; i++){
		baseAddrs = &mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize + mScheme.field_offset[i]];
		// write column
		if (mScheme.field_type[i] > 0){
			double dField = getColumnValue<double>(record, i);
			memcpy(baseAddrs, &dField, sizeof(double));
		}
		else{
			int iField = getColumnValue<int>(record, i);
			memcpy(baseAddrs, &iField, sizeof(int));
		}
	}
	mNumRows++;
}

void RowTable::insertRecord(int xid, string record, LogManager* logger){
	unique_lock<mutex> lock(table_mutex);
	logger->addLogBuffer(to_string(xid)+"|INSERT|"+record);
    // if last page is full, build a new page
    if(mNumRows % PAGE_SIZE == 0){
        pageBuffer = (TablePage*)malloc(sizeof(TablePage));
        pageBuffer->records = (char*)malloc(mScheme.recordSize * PAGE_SIZE);
        mTable.push_back(pageBuffer);
        mNumRows = 0;
    }
    /********************************************************************
     * Data format in the TablePage                                     *
     * [pointer0][pointer1][pointer2]... [column0][column1][column2]... */
    
     //Target address ::: mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize];
	char* baseAddrs;
	string val;
	for (int i = 0; i < mScheme.field_count; i++){
		baseAddrs = &mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize + mScheme.field_offset[i]];
		// write column
		val = splitString(record);	
		if (mScheme.field_type[i] > 0){
			double dField = atof(val.c_str()); 
			memcpy(baseAddrs, &dField, sizeof(double));
		}
		else{
			int iField = atoi(val.c_str());
			memcpy(baseAddrs, &iField, sizeof(int));
		}
	}
	char* value = &mTable[mTable.size()-1]->records[mNumRows * mScheme.recordSize + mScheme.field_offset[0]];
	int key;
	memcpy(&key, value, sizeof(int));
	// insert index
	bptIndex.Insert(key, value);
	mNumRows++;
}
/* param
 * col: Column to scan for
 * val: Filter value. Print records below this value
 */
void RowTable::scan(int col, int filter_value){
    int count = 0;
    for(int page = 0; page < mTable.size(); page++){
        int end = (page+1 == mTable.size())? mNumRows : PAGE_SIZE;
        for(int rec = 0; rec < end; rec++){
            int val = getColumnValue<int>(&mTable[page]->records[rec * mScheme.recordSize], col);
            if (val < filter_value){
                printRecord(&mTable[page]->records[rec*mScheme.recordSize]);
                count ++;
            }
        }
    }
    cout << count << " records found!" << endl;
}
void RowTable::scan(int col, double filter_value){
    int count = 0;
    for(int page = 0; page < mTable.size(); page++){
        int end = (page+1 == mTable.size())? mNumRows : PAGE_SIZE;
        for(int rec = 0; rec < end; rec++){
            double val = getColumnValue<double>(&mTable[page]->records[rec * mScheme.recordSize], col);
            if (val < filter_value){
                printRecord(&mTable[page]->records[rec*mScheme.recordSize]);
                count ++;
            }
        }
    }
    cout << count << " records found!" << endl;
}

// Ranged scan on integer key
void RowTable::scan(int col, int begin_key, int end_key){
	int count = 0;
	for (int page = 0; page < mTable.size(); page++){
		int end = (page + 1 == mTable.size()) ? mNumRows : PAGE_SIZE;
		for (int rec = 0; rec < end; rec++){
			int val = getColumnValue<int>(&mTable[page]->records[rec * mScheme.recordSize], col);
			if (val > end_key){
				return;
			}
			if (val >= begin_key){
				printRecord(&mTable[page]->records[rec*mScheme.recordSize]);
				count++;
			}
		}
	}
	cout << count << " records found!" << endl;
}

void RowTable::bulkload(){
    int key;
    char* addrs;
    
    for (int page= 0; page < mTable.size(); page++){
        int end = (page+1 == mTable.size())? mNumRows :PAGE_SIZE;
        for (int rec = 0; rec < end; rec++){
            // get pointer to this record, get value of the search key, then insert to the tree
			addrs = &mTable[page]->records[rec * mScheme.recordSize];
						
			// We only accept integer as first column
			memcpy(&key, addrs, sizeof(int));
            																				 
            // Insert sorted keys into the right most leafnode
            bptIndex.InsertRightMost(key, addrs);
        }
    }
    cout << "total "<<mTable.size()<<" pages, ";
    cout << "last page with "<< mNumRows << " records"<<endl;
}

void RowTable::readScheme(string& line){
    int count=0;
    vector<string> toks;
    while(line.size() > 0){
        toks.push_back(splitString(line));
        count++;
    }
    mScheme.field_count = count;
    mScheme.field_type = (int*)malloc(sizeof(int)*count);
	mScheme.field_offset = (int*)malloc(sizeof(int)*count);
	int sum_offset = 0;
    for(int i =0; i < toks.size(); i++){
        mScheme.field_type[i] = getType(toks[i]);
		mScheme.field_offset[i] = sum_offset;
		sum_offset += (mScheme.field_type[i] > 0) ? sizeof(double) : sizeof(int);
	}
	mScheme.recordSize = sum_offset;
}

template<typename T> T
RowTable::getColumnValue(char* record, int col){
	T ret_val;
	unsigned long sizeofField = (mScheme.field_type[col] > 0) ? sizeof(double) : sizeof(int);
	memcpy(&ret_val, &record[mScheme.field_offset[col]], sizeofField);
	return ret_val;     
}


void RowTable::searchIndex(int key){
    vector<char*> targets;
    bptIndex.SearchAll(key, &targets);
    for (int i = 0; i < targets.size(); i++){
        printRecord(targets[i]);
    }
	fprintf(ofs, "%d records found\n", targets.size() );
	fflush(ofs);
}

void RowTable::buildHashTable(Hashmap* hashT){
    int key;
    char* addrs;
    for (int page= 0; page < mTable.size(); page++){
        int end = (page+1 == mTable.size())? mNumRows :PAGE_SIZE;
        for (int rec = 0; rec < end; rec++){
            // get pointer to this record, get value of the search key, then insert to the tree
			addrs = &(mTable[page]->records[rec* mScheme.recordSize]);
			memcpy(&key, addrs, sizeof(int));

            // Insert sorted keys into the hashmap
            hashT->insert(Hashmap::value_type(key, addrs));
        }
    }
}

/********************************************************************
 * Join with the given inTable and returns a new result table pointer
 * Join output table records are not neccessarily inserted in sorted order
 * but for the sake of complexity, assume it is sorted
 ********************************************************************/
RowTable* RowTable::equiJoin(int self_col, RowTable* inTable, int in_col){
    Hashmap hash;
    // Create join output table and initialize appropriate scheme
    Scheme joinedScheme;
    joinedScheme.field_count = mScheme.field_count + inTable->mScheme.field_count;
    joinedScheme.field_type = (int*)malloc(sizeof(int) * joinedScheme.field_count);
	joinedScheme.field_offset = (int*)malloc(sizeof(int)* joinedScheme.field_count);
    int jointype = 0;
	int sum_offset = 0;
	for (int my = 0; my < mScheme.field_count; my++){
		joinedScheme.field_type[jointype] = mScheme.field_type[my];
		joinedScheme.field_offset[jointype] = sum_offset;
		sum_offset += (joinedScheme.field_type[jointype++] > 0) ? sizeof(double) : sizeof(int);
	}
	for (int in = 0; in < inTable->mScheme.field_count; in++){
		joinedScheme.field_type[jointype] = inTable->mScheme.field_type[in];
		joinedScheme.field_offset[jointype] = sum_offset;
		sum_offset += (joinedScheme.field_type[jointype++] > 0) ? sizeof(double) : sizeof(int);
	}																																											
    joinedScheme.recordSize = mScheme.recordSize + inTable->mScheme.recordSize;
    
    RowTable* joinedT = new RowTable(joinedScheme, ofs);
    
    /*************************************
     Build hash table on smaller table   *
     (Assume inTable is smaller)         */
    inTable->buildHashTable(&hash);
    
    /*************************************
     Test hash on larger table           */
    int key;
    char* addrs;
    char* buf = (char*)malloc(joinedScheme.recordSize);            // record join buffer
    
	int dataidx;
	int ptridx;
	unsigned long typeSize = 0;

	// for each record in this table,
	for (int page = 0; page < mTable.size(); page++){
		int end = (page + 1 == mTable.size()) ? mNumRows : PAGE_SIZE;
		for (int rec = 0; rec < end; rec++){
			addrs = &mTable[page]->records[rec * mScheme.recordSize];
			memcpy(&key, addrs, sizeof(int));

			// find hash bucket, join with all entries in the bucket
			auto its = hash.equal_range(key);
			for (auto it = its.first; it != its.second; ++it){

				int joinedColumnIdx = 0;
				// 1. Write mytable to buffer
				for (int my = 0; my < mScheme.field_count; my++, joinedColumnIdx++){
					if (mScheme.field_type[my] > 0){
						double dField = getColumnValue<double>(addrs, my);
						memcpy(&buf[joinedScheme.field_offset[joinedColumnIdx]], &dField, sizeof(double));
					}
					else{
						int iField = getColumnValue<int>(addrs, my);
						memcpy(&buf[joinedScheme.field_offset[joinedColumnIdx]], &iField, sizeof(int));
					}
				}
				// 2. Write inTable to buffer
				for (int in = 0; in < inTable->mScheme.field_count; in++, joinedColumnIdx++){
					if (inTable->mScheme.field_type[in] > 0){
						double dField = inTable->getColumnValue<double>(it->second, in);
						memcpy(&buf[joinedScheme.field_offset[joinedColumnIdx]], &dField, sizeof(double));
					}
					else{
						int iField = inTable->getColumnValue<int>(it->second, in);
						memcpy(&buf[joinedScheme.field_offset[joinedColumnIdx]], &iField, sizeof(int));
					}
				}
				// insert to joinedT
				joinedT->insertRecord(buf);
			}
		}
	}
    // build index on the joinedT, since we assume table is built in sorted order, we bulkload
    // joinedT->bulkload();
	free(buf);
    return joinedT;
}

void RowTable::printRecord(char* addrs){
	fprintf(ofs, "|");
    for(int col = 0; col < mScheme.field_count; col++){
        //memcpy(&buf, addrs+(col*sizeof(char*)), sizeof(char*));
        if(mScheme.field_type[col] > 0)
            fprintf(ofs, "%.2f|", getColumnValue<double>(addrs, col));
        else
            fprintf(ofs, "%d|", getColumnValue<int>(addrs, col));
    }
	fprintf(ofs, "\n");
	fflush(ofs);
}
