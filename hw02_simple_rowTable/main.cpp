//
//  main.cpp
//  DBhw02
//  Wooin Lee
//  2014-22572
//
#include <iostream>
#include <chrono>
#include <random>

#include "RowTable.h"

#define LINE cout<<"--------------------------------------------------"<<endl;
#define FLINE fprintf(ofs, "--------------------------------------------------\n");

using namespace std;
int menu;
int main(int argc, const char * argv[]) {
    if(argc < 2){
        cout << "Usage : [datafile path]" << endl;
        cout << "Usage : [datafile1 path] [datafile2 path]" << endl;
        return 1;
    }else if(argc == 4){
			menu = atoi(argv[3]);
    }
		FILE* ofs = fopen("output.csv", "a");
    RowTable* table1 = new RowTable(ofs);
    cout << "loading datafile1..."<<endl;
    table1->load(argv[1]);
    
    cout << "bulk loading table on to the bpt index..."<<endl;
    table1->bulkload();
    LINE
    RowTable* table2 = new RowTable(ofs);
    cout << "loading datafile2..."<<endl;
    table2->load(argv[2]);
    
    cout << "bulk loading table on to the bpt index..."<<endl;
    table2->bulkload();
    
    RowTable* jointable;
    switch(menu){
        case 1:{
            LINE
			FLINE
            cout << "HW2 : scan on 56789.12" << endl;
			fprintf(ofs, "HW2 : scan on 56789.12\n");
            table2->scan(2, 56789.12);
            break;
        }
        case 2:{
            LINE
			FLINE
            cout << "HW2 : search key 1000" << endl;
		    fprintf(ofs, "HW2 : search key 1000\n");
            table1->searchIndex(1000);
            
            LINE
			FLINE
            cout << "HW2 : search key 2000" << endl;
			fprintf(ofs, "HW2 : search key 2000\n");
            table1->searchIndex(2000);
            
            LINE
			FLINE
            cout << "HW2 : search key 3000" << endl;
			fprintf(ofs, "HW2 : search key 3000\n");
            table1->searchIndex(3000);
            
            LINE
			FLINE
            cout << "HW2 : search key 4000" << endl;
						fprintf(ofs, "HW2 : search key 4000\n");
            table1->searchIndex(4000);
            
            LINE
						FLINE
            cout << "HW2 : search key 8000" << endl;
						fprintf(ofs, "HW2 : search key 8000\n");
            table1->searchIndex(8000);
            break;
        }
        case 3: {
            LINE
						FLINE
            cout << "HW2 : search random key 10000 times" << endl;
						fprintf(ofs, "HW2 : search random key 10000 times\n");
            srand(9);
            int key;
            const auto begin = chrono::high_resolution_clock::now();
            for(int i = 0; i < 10000; i++){
                key =  rand()% 6000000 + 1;
                table2->searchIndex(key);
            }
            auto time = chrono::high_resolution_clock::now() - begin;
            std::cout << "Elapsed time: " << chrono::duration<double, std::milli>(time).count() << " ms\n";
						fprintf(ofs, "Elapsed time: %f ms\n", chrono::duration<double, std::milli>(time).count());
            break;
        }
        case 4:{
            LINE
						FLINE
            cout << "HW2 : join LINEITEM & ORDERS" << endl;
            cout << "Output joined results of 40-60"<< endl;
						fprintf(ofs, "HW2 : join LINEITEM & ORDERS : 40-60\n");
            jointable = table2->equiJoin(0, table1, 0);
						jointable->scan(0, 40, 60);
            
            delete jointable;
            break;
        }
    }
		LINE

		fclose(ofs);
    delete table1;
    delete table2;
    return 0;
}
