//
// server_main.cc
// DBhw05
// Wooin Lee
// 2014-22572
//
#include "ServerSocket.h"
#include "SocketException.h"

#include "ThreadPool.h"
#include "RowTable.h"

#include "Util.h"

#include "XactManager.h"
#include "LogManager.h"

#include "MVCC.h"

#include <string>
#include <iostream>

#define NUM_THREAD 4

using namespace std;

int main(int argc, char* argv[]) {
	// query results will be written
	FILE* ofs = fopen("output.txt", "a");
	RowTable* DB = new RowTable(ofs);
	
	bool isFirst = true;
	bool gracefulExit = false;
	
	string operation;
	string data;
    try {
        // Create the Socket
        ServerSocket server(30000);
		
		// Initialize thread pool with N worker threads
		ThreadPool worker_pool(NUM_THREAD);
		ThreadPool flush_thread(1);
		ThreadPool version_thread(1);

		XactManager* xmanager = new XactManager();
		LogManager* logger = new LogManager();
		MVCC* mvcc = new MVCC();
		// bind MVCC and the row table
		mvcc->bindTable(DB);
		DB->bindVersionSpace(mvcc);

		// dedicate a thread for flushing log buffer to a log file
		flush_thread.enqueue([logger]{logger->runFlushThread();});
		// dedicate a thread for garbage collection & version management
		version_thread.enqueue([mvcc]{mvcc->runGCThread();});

        while(!gracefulExit) {
            ServerSocket new_sock;
            server.accept(new_sock);

            try {
                while(true) {
                    new_sock >> data;
                    cout << "[Received]\t" << data << endl;
					/***************
					 * Actual work *
					 ***************/
                    operation = splitString(data);
					// on receiving the first transaction, scheme should be built first
                    if(isFirst){
						string data_bak = data;
                    	DB->readScheme(data_bak);
                    	isFirst = false;
					}
                    // Invoke a worker thread from the thread pool
                    // The server will enqueue a task and walk away, waiting for any incoming tasks
                    if(operation == "INSERT"){
                    	worker_pool.enqueue([data, xmanager, logger, DB]{
							int xid = xmanager->getXid();
							logger->addLogBuffer(to_string(xid)+"|INIT|"+to_string(xmanager->getInitTime(xid)));

							DB->insertRecord(xid, data, xmanager, logger);
						});
					}
					else if(operation == "UPDATE"){
						int lineNum = stoi(splitString(data));
						worker_pool.enqueue([lineNum, data, xmanager, logger, DB]{
							int xid = xmanager->getXid();
							logger->addLogBuffer(to_string(xid)+"|INIT|"+to_string(xmanager->getInitTime(xid)));
							
							DB->update(xid, lineNum, data, xmanager, logger);
						});
					}
					else if(operation == "SCAN"){
						// col = 2, filter_value[double]
						worker_pool.enqueue([data, xmanager, logger, DB]{
							int xid = xmanager->getXid();
							int filter_val = std::stoi(data);
							logger->addLogBuffer(to_string(xid)+"|INIT|"+to_string(xmanager->getInitTime(xid)));

							DB->scan(xid, 2, filter_val, xmanager,logger);
						});
					}
					else if(operation == "SNAPSHOT"){
						while(worker_pool.getNumTasks() > 0){}// block wait until currently running xacts are done
						int xid = xmanager->getXid();
						logger->addLogBuffer("SNAPSHOT|S");
						DB->snapshot(xid, logger);
						logger->addLogBuffer("SNAPSHOT|F");
					}
					// send ACK back to the client so the client can continue sending streams
					// ACK does not mean that the request has been completed, but only as a meaning that the request has been 
					// received correctly without any network error
                    new_sock << "ACK\n";
                }
            } catch(SocketException&) {}
        }
    } catch(SocketException& e) {
        std::cout<< "Exception caught: " << e.description() << std::endl;
    }
    fclose(ofs);
    delete DB;
    return 0;
}
