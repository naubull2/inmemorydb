//
// Util.h
// DBhw04
// Wooin Lee
// 2014-22572
//
#ifndef Util_h
#define Util_h

#include <string>

using namespace std;

bool isDouble(string & str);

int getType(string & str);

string splitString(string& str);

#endif
