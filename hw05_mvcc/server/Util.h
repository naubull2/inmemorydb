//
// Util.h
// DBhw04
// Wooin Lee
// 2014-22572
//
#ifndef Util_h
#define Util_h

#include <string>

using namespace std;

typedef struct RID{
	int page;
	int rec;
	bool operator<(const RID& rhs) const{
		return rhs.page< this->page || (rhs.page==this->page && rhs.rec < this->rec);
	}
	bool operator>(const RID& rhs) const{
		return rhs.page> this->page || (rhs.page==this->page && rhs.rec > this->rec);
	}
	bool operator==(const RID& rhs) const{
		return rhs.page == this->page && rhs.rec==this->rec;
	}
}RID;

bool isDouble(string & str);

int getType(string & str);

string splitString(string& str);

#endif
