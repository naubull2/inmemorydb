//
//  RowTable.h
//  DBhw05
//  Wooin Lee
//  2014-22572
//

#ifndef RowTable_h
#define RowTable_h

#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <unordered_map>
#include <mutex>

#include "BPlusTree.h"
#include "LogManager.h"
#include "XactManager.h"
#include "Util.h"


#define PAGE_SIZE 1000
#define TREE_ORDER 50

typedef struct TablePage{
    char* records;
}TablePage;


typedef struct Scheme{
    int field_count;
    int* field_type;
    int recordSize;     // actual record size
	int* field_offset;	// offset of each field
}Scheme;

typedef unordered_multimap<int, char*> Hashmap;

class MVCC;
class RowTable{
public:
	FILE* ofs;
    Scheme mScheme;
	std::vector<TablePage*> mTable;
    TablePage* pageBuffer;
    int mNumRows;   // number of records in the last TablePage in the page list
    
    RowTable(FILE* ofs_);
    RowTable(Scheme scheme_, FILE* ofs_);                // create table with given scheme
    ~RowTable();

	void bindVersionSpace(MVCC* vs);	// bind a version controller with the table

    void load(std::string path);                  // load table from the specified file path
    
    void insertRecord(char* pRecord);        // insert a single record, assuming sorted order of pkey
    void insertRecord(int xid, std::string str, XactManager* xmanager, LogManager* logger);
    
    void scan(int xid, int col, int filter_value, XactManager* xmanager, LogManager* logger);    // scan filter rows by the given value
    void scan(int xid, int col, double filter_value, XactManager* xmanager, LogManager* logger);
	void scan(int col, int begin_key, int end_key); // range scan for joined output

	void update(int xid, int line, std::string str, XactManager* xmanager, LogManager* logger);
	void applyVersion(RID rid, std::string str);

	void snapshot(int xid, LogManager* logger);

    void bulkload();
    
    // Simple scheme reader submodules
    void readScheme(std::string & line);

    void printRecord(char* addrs);
    void searchIndex(int key);
    
    void buildHashTable(Hashmap* hashT);    // build hash table
    
    template<typename T> T getColumnValue(char* record, int col);
    
    // build hash table on the inTable, then output on hash hit
    RowTable* equiJoin(int self_col, RowTable* inTable, int in_col);

private:
    //ofstream ofs;
    BPlusTree<int, char*, TREE_ORDER> bptIndex;
	MVCC* mvcc;
    mutex table_mutex;
    
};


#endif /* RowTable_h */
