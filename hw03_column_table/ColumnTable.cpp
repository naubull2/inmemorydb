//
//  ColumnTable.cpp
//  DBhw03
//  Wooin Lee
//  2014-22572
//
#include "ColumnTable.h"

ColumnTable::ColumnTable(RowTable* rowTable){
	ofs = rowTable->ofs;
	// build scheme
	
	n_intDict = n_doubleDict = 0;

	mScheme.field_count = rowTable->mScheme.field_count;
	mScheme.field_type = (int*)malloc(sizeof(int)*mScheme.field_count);
	for (int i = 0; i < mScheme.field_count; i++){
		mScheme.field_type[i] = rowTable->mScheme.field_type[i];
		if (mScheme.field_type[i] > 0){
			n_doubleDict++;
		}
		else{
			n_intDict++;
		}
	}
	// initialize columns
	mColumns = (Column*)malloc(sizeof(Column)*mScheme.field_count);
	/*******************************************
	Copy record values to appropriate columns */

	/* Well... I don't think this is possible for this assignment unless I hard code bit length
	// 1. Initialize each column's bitsize */

	// 1. Initialize columns with single page each
	for (int col = 0; col < mScheme.field_count; col++){
		// at least 3 byte is needed for this assignment, so we use 4byte int
		mColumns[col].recordSize = sizeof(int) + sizeof(int);
		//pageBuffer = new ColumnPage();
		pageBuffer = (ColumnPage*)malloc(sizeof(ColumnPage));

		pageBuffer->records = (char*)malloc(mColumns[col].recordSize * COL_PAGE_SIZE);
		mColumns[col].pages = new vector < ColumnPage* >;
		mColumns[col].pages->push_back(pageBuffer);
		mColumns[col].numRecord = 0;

	}

	/****************************************
	 1. Build dictionary for each column		*/
	//intDict = (intDictionary*)malloc(sizeof(intDictionary)*n_intDict);
	//doubleDict = (doubleDictionary*)malloc(sizeof(doubleDictionary)*n_doubleDict);
	intDict = new intDictionary[n_intDict];
	doubleDict = new doubleDictionary[n_doubleDict];

	intRehash = new intintRehash[n_intDict];
	doubleRehash = new intdoubleRehash[n_doubleDict];
			
	int total_records = rowTable->mTable.size() * 1000 + rowTable->mNumRows;

	/*
	// initialize with max slots of [total_records] : in the worst case
	for (int i = 0; i < mScheme.field_count; i++){
		if (mScheme.field_type[i] > 0){
			buildDictionary(&doubleDict[getDictId(i)], total_records);
		} 
		else{
			buildDictionary(&intDict[getDictId(i)], total_records);
		}
	}
	*/

	char* addrs;
	int key, index, dictId;
	int val_i;
	double val_d;
	// scan through all records of rowTable once to fill the dictionaries
	for (int page = 0; page < rowTable->mTable.size(); page++){
		int end = (page + 1 == rowTable->mTable.size()) ? rowTable->mNumRows : PAGE_SIZE;
		if ((page+1) % 100 == 0)
			cout << (page+1) <<"\r";
		for (int rec = 0; rec < end; rec++){
			addrs = &rowTable->mTable[page]->records[rec * rowTable->mScheme.recordSize];
			memcpy(&key, addrs, sizeof(int));

			for (int i = 0; i < mScheme.field_count; i++){
				dictId = getDictId(i);
				if (mScheme.field_type[i] > 0){
					val_d = rowTable->getColumnValue<double>(addrs, i);
					
					insertDictionary(&doubleDict[dictId], &doubleRehash[dictId], val_d);
					
					index = getIndex(&doubleDict[dictId], val_d);
					insertRecordCol(&mColumns[i], key, index);
				}
				else{
					val_i = rowTable->getColumnValue<int>(addrs, i);
					
					insertDictionary(&intDict[dictId], &intRehash[dictId], val_i);
					
					index = getIndex(&intDict[dictId], val_i);
					insertRecordCol(&mColumns[i], key, index);
				}
			}
		}
	}// end dictionary fill-up
	cout << endl;
}
ColumnTable::~ColumnTable(){
	free(mScheme.field_type);
	for (int i = 0; i < mScheme.field_count; i++){
		for (int page = 0; page < mColumns[i].pages->size(); page++){
			free((*mColumns[i].pages)[page]);
		}																							
		free(mColumns[i].pages);
	}
	
	free(mColumns);
	delete[] intDict;
	delete[] doubleDict;
	delete[] intRehash;
	delete[] doubleRehash;
}

void ColumnTable::scan(int col, double filter_value){
	int count = 0;
	Column* column = &mColumns[col];
	intdoubleRehash* dictionary = &doubleRehash[getDictId(col)];

	double val;
	int key, idx;
	for (int page = 0; page < column->pages->size(); page++){
		int end = (page + 1 == column->pages->size()) ? column->numRecord : COL_PAGE_SIZE;
		cout << "currently scanning page " << page << "\r";
		for (int rec = 0; rec < end; rec++){
			memcpy(&idx, &(*column->pages)[page]->records[rec * column->recordSize], sizeof(int));
			val = getValue(dictionary, idx);
			if (val < filter_value){
				printRecord(page, rec);
				count++;
			}
		}
	}		
	cout << endl << count << " records found!!" << endl;
}																					

double ColumnTable::average1(int col){
	Column* column = &mColumns[col];
	double avg_ = 0.0;
	int count = 0;
	int idx;
	for (int page = 0; page < column->pages->size(); page++){
		int end = (page + 1 == column->pages->size()) ? column->numRecord : COL_PAGE_SIZE;
		for (int rec = 0; rec < end; rec++){
			memcpy(&idx, &(*column->pages)[page]->records[rec*column->recordSize], sizeof(int));
			avg_ += getValue(&doubleRehash[getDictId(col)], idx);
			count++;
		}																										 
	}	
	return avg_ /= count;
}

double ColumnTable::average2(int col){
	Column* column = &mColumns[col];
	int idx;
	int count = 0;
	doubleDictionary* dictionary = &doubleDict[getDictId(col)];
	int* counter = (int*)calloc(dictionary->size(), sizeof(int));
	double avg_ = 0.0;
	for (int page = 0; page < column->pages->size(); page++){
		int end = (page + 1 == column->pages->size()) ? column->numRecord : COL_PAGE_SIZE;
		for (int rec = 0; rec < end; rec++){
			memcpy(&idx, &(*column->pages)[page]->records[rec*column->recordSize], sizeof(int));
			counter[idx]++;
			count++;
		}
	}
	for (int i = 0; i < dictionary->size(); i++){
		avg_ += getValue(&doubleRehash[getDictId(col)], i) * counter[i];
	}
	free(counter);
	return avg_ /= count;
}


int ColumnTable::getDictId(int col){
	int idx_int = 0;
	int idx_double = 0;
	for (int i = 0; i < col; i++){
		if (mScheme.field_type[i] > 0)	
			idx_double++;
		else
			idx_int++;
	}
	// Find the dictionary index, depending on its values' data type
	return (mScheme.field_type[col] > 0) ? idx_double : idx_int;
}

/*
void ColumnTable::buildDictionary(intDictionary* dict, int total_size){
	dict->dict = (int*)malloc(sizeof(int) * total_size);
	dict->dictionary_size = 0; 
}

void ColumnTable::buildDictionary(doubleDictionary* dict, int total_size){
	dict->dict = (double*)malloc(sizeof(double) * total_size);
	dict->dictionary_size = 0;
}
*/

void ColumnTable::insertDictionary(intDictionary* dict, intintRehash* rehash, int val){
	// insert value and increment value count
	// search hash, 
	auto its = dict->find(val);
	if (its == dict->end()){
		int index = (int)dict->size();
		(*dict)[val] = index;
		(*rehash)[index] = val;		// for constant dictionary look up
	}
}

void ColumnTable::insertDictionary(doubleDictionary* dict, intdoubleRehash* rehash, double val){
	string strkey = to_string_precision(val);
	auto its = dict->find(strkey);
	if (its == dict->end()){
		int index = (int)dict->size();
		(*dict)[strkey] = index;
		(*rehash)[index] = val;
	}
}


int ColumnTable::getIndex(intDictionary* dict, int val){
	auto its = dict->find(val);
	if (its == dict->end())
		return -1;
	else
		return its->second;
}

int ColumnTable::getIndex(doubleDictionary* dict, double val){
	string strkey = to_string_precision(val);
	auto its = dict->find(strkey);
	if (its == dict->end())
		return -1;
	else
		return its->second;
}

int ColumnTable::getValue(intintRehash* dict, int idx){
	auto its = dict->find(idx);
	return its->second;
}

double ColumnTable::getValue(intdoubleRehash* dict, int idx){
	auto its = dict->find(idx);
	return its->second;
}


/*
template<typename T> 
int ColumnTable::getRequiredBits(Dictionary<T>* dict){
	int n = 1;
	int range = 2;
	while (range < dict->dictionary_size){
		n++;;
		range *= 2;
	}
	return n;
}
*/

void ColumnTable::insertRecordCol(Column* column, int index, int val){
	// if last page is full, build a new page
	if (column->numRecord == COL_PAGE_SIZE){
		pageBuffer = new ColumnPage();
		pageBuffer->records = (char*)malloc(column->recordSize * COL_PAGE_SIZE);
		column->pages->push_back(pageBuffer);
		column->numRecord = 0;
	}
	char* baseAddrs = &(*column->pages)[column->pages->size() - 1]->records[column->numRecord * column->recordSize];
	// write single key, value
	memcpy(baseAddrs, &val, sizeof(int));
	baseAddrs = &(*column->pages)[column->pages->size() - 1]->records[column->numRecord * column->recordSize + sizeof(int)];
	memcpy(baseAddrs, &index, sizeof(int));

	column->numRecord++;	
}

void ColumnTable::printRecord(int page, int idx){
	char* addrs;
	int dictId;
	int vali;
	double vald;
	fprintf(ofs, "|");
	for (int i = 0; i < mScheme.field_count; i++){
		addrs = &(*mColumns[i].pages)[page]->records[idx * mColumns[i].recordSize];
		memcpy(&dictId, addrs, sizeof(int));
		if (mScheme.field_type[i] > 0){
			vald = getValue(&doubleRehash[getDictId(i)], dictId);
			fprintf(ofs, "%.2f|", vald);
		}
		else{
			vali = getValue(&intRehash[getDictId(i)], dictId);
			fprintf(ofs, "%d|", vali);
		}
	}
	fprintf(ofs, "\n");
}


string ColumnTable::to_string_precision(double val)
{
	std::ostringstream out;
	out.precision(2);
	out << std::fixed << val;
	return out.str();
}