//
// CBuffer.h
// DBhw04
// Wooin Lee
// 2014-22572
//
// A simple implementation of circular queue buffer with locking
#ifndef CBuffer_h
#define CBuffer_h

#include <mutex>
#include <stdlib.h>

#define Q_SIZE 4096 

class CBuffer{
	private:
		int item[Q_SIZE];
		int head;
		int tail;
		int qsize;
		std::mutex buffer_mutex;
	public:
		CBuffer();
		~CBuffer();
		void enqueue(int);
		int dequeue();
		int size();
		bool isEmpty();
		bool isFull();
};

#endif /* CBuffer_h */
