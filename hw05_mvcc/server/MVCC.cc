//
// MVCC.cc
// DBhw05
// Wooin Lee
// 2014-22572
//
#include "MVCC.h"
#include <iostream>
using namespace std;

MVCC::MVCC(){
	isStop = false;
}
MVCC::~MVCC(){
	isStop = true;
}

void MVCC::bindTable(RowTable* db_){
	db = db_;
}

void MVCC::createVersion(RID rid, string data){
	Version newVersion;
	newVersion.TS = clock();
	newVersion.data = data;

	unique_lock<mutex> lock(versionSpace_mutex);
	if(GClock.find(rid) != GClock.end()){
		while(GClock[rid] > 0){}
	}
	versionSpace[rid].push_back(newVersion);
}

void MVCC::runGCThread(){
	int gc_frequency = 50000;	// check buffer every 50000 cpu time
	while(true){
		if(isStop) break;
		////
		// iterate through the map, check current time and if the last version in a list is older than 30000,
		// gc the versions,
		// apply the latest to the RowTable [ this is locked by mutex ]
		// remove the version space
		for(auto const &entry : versionSpace){
			unique_lock<mutex> lock(versionSpace_mutex);
			if(clock() - entry.second.back().TS > gc_frequency){
				string data = entry.second.back().data;
				if(GClock.find(entry.first)!=GClock.end()){// wait untill no one read/write
					while(GClock[entry.first] > 0){}
				}
				versionSpace.erase(entry.first); // GC
				// db->updateTableVersion();
				db->applyVersion(entry.first, data);
			}// unlocked
		}
	}
}

bool MVCC::checkVersion(RID rid){
	if(versionSpace.find(rid) == versionSpace.end()){
		// not found
		return false;
	}else{
		//found
		return true;
	}	
}
// read designated column only
string MVCC::readLatestVersion(RID rid, int time_stamp, int col){
	// prevent (GC call/write new version) while someone's reading
	// may crash the iterator
	if(GClock.find(rid) == GClock.end()){
		GClock[rid] = 1;
	}else{
		GClock[rid] += 1;
	}
	string result = "";
	for(auto it = versionSpace[rid].rbegin(); it != versionSpace[rid].rend(); ++it){
		if(it->TS < time_stamp){ // The first version whose TS is less than my TS(latest committed)
			result = it->data;
			break;
		}
	}
	string colValue = "";
	for(int i = 0; i <= col; i++)
		colValue = splitString(result);	
	GClock[rid] -= 1;
	return colValue; 
}

// read whole record
string MVCC::readLatestVersion(RID rid, int time_stamp){
	// prevent (GC call/write new version) while someone's reading
	// may crash the iterator
	if(GClock.find(rid) == GClock.end()){
		GClock[rid] = 1;
	}else{
		GClock[rid] += 1;
	}
	string result = "";
	for(auto it = versionSpace[rid].rbegin(); it != versionSpace[rid].rend(); ++it){
		if(it->TS < time_stamp){ // The first version whose TS is less than my TS(latest committed)
			result = it->data;
			break;
		}
	}
	GClock[rid] -= 1;
	return result; 
}

void MVCC::versionPrint(FILE* fp){
	unique_lock<mutex> lock(versionSpace_mutex);
	int linenumber;
	for(auto const &entry :versionSpace){
		linenumber = (int)(entry.first.page * PAGE_SIZE + entry.first.rec);
		for(auto const &version : entry.second){
			fprintf(fp, "%d at %d, %s\n",linenumber, version.TS,  version.data.c_str());
		}
		fflush(fp);
	}
}

