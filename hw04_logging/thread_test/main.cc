#include <iostream>
#include <string>
#include "ThreadPool.h"

using namespace std;

void workerFunction1(int a){
	cout << "hello " << a << " world!" <<endl;
}

void workerFunction2(string str){
	cout << str <<endl;
}


int main(void){

	ThreadPool pool(4);
	string tmp_str = "@@@@@@@@@@@@@@@@@";

	for(int i = 0; i < 20; i++){
		if(i < 10){
			pool.enqueue([i]{
					workerFunction1(i);});
		}
		else{
			pool.enqueue([tmp_str]{
					workerFunction2(tmp_str);});
		}
	}

	getchar();
	return 0;
}
