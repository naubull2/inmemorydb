//
//  main.cpp
//  DBhw02
//  Wooin Lee
//  2014-22572
//
#include <iostream>
#include <chrono>
#include <random>

#include "RowTable.h"
#include "ColumnTable.h"

#define LINE cout<<"--------------------------------------------------"<<endl;
#define FLINE fprintf(ofs, "--------------------------------------------------\n");

using namespace std;
int menu;
int main(int argc, const char * argv[]) {
	if (argc < 2){
		cout << "Usage : [datafile path]" << endl;
		return 1;
	}
	else if (argc == 3){
		menu = atoi(argv[2]);
	}
	FILE* ofs = fopen("output.csv", "a");
	RowTable* table1 = new RowTable(ofs);
	cout << "loading datafile1..." << endl;
	table1->load(argv[1]);

	cout << "bulk loading table on to the bpt index..." << endl;
	table1->bulkload();
	LINE

	ColumnTable* columnT = new ColumnTable(table1);
	delete table1;
	switch (menu){
	case 1:{
			LINE
			FLINE
			cout << "HW3 : scan on 56789.12" << endl;
			fprintf(ofs, "HW3 : scan on 56789.12\n");
			columnT->scan(2, 56789.12);
			break;
		}
	case 2:{
			LINE
			FLINE
			cout << "HW3 : average, look up dictionary every time" << endl;
			fprintf(ofs, "HW3 : average, look up dictionary every time\n");
			const auto begin = chrono::high_resolution_clock::now();
			double avg1_ = columnT->average1(2);
			auto time = chrono::high_resolution_clock::now() - begin;
			fprintf(ofs, "Average1: %.2f\n", avg1_);
			std::cout << "Elapsed time: " << chrono::duration<double, std::milli>(time).count() << " ms\n";
			fprintf(ofs, "Elapsed time: %f ms\n", chrono::duration<double, std::milli>(time).count());
			
			LINE
			FLINE
			cout << "HW3 : average, count vid, then look up dictionary" << endl;
			fprintf(ofs, "HW3 : average, count vid, then look up dictionary\n");
			const auto begin2 = chrono::high_resolution_clock::now();
			double avg2_ = columnT->average2(2);
			auto time2 = chrono::high_resolution_clock::now() - begin2;
			fprintf(ofs, "Average2: %.2f\n", avg2_);
			std::cout << "Elapsed time: " << chrono::duration<double, std::milli>(time).count() << " ms\n";
			fprintf(ofs, "Elapsed time: %f ms\n", chrono::duration<double, std::milli>(time).count());

			break;
		}
	}
	LINE

	fclose(ofs);
	delete columnT;
	return 0;

}
