//
// CBuffer.cc
// DBhw04
// Wooin Lee
// 2014-22572
//
#include "CBuffer.h"
#include <math.h>

CBuffer::CBuffer(){
	head = 0;
	tail= 0;
	qsize = 0;
}
void CBuffer::enqueue(int data){
	std::unique_lock<std::mutex> lock(buffer_mutex);
	item[tail] = data;
	tail = (tail+1)%Q_SIZE;
	qsize++;
}

int CBuffer::dequeue(){
	std::unique_lock<std::mutex> lock(buffer_mutex);
	int data = item[head];
	head = (head+1)%Q_SIZE;
	qsize--;
	return data;
}

int CBuffer::size(){
	return qsize;
}

bool CBuffer::isEmpty(){
	if(head==tail){
		return true;
	}else{
		return false;
	}
}
bool CBuffer::isFull(){
	if(head==(tail+1)%Q_SIZE)
		return true;
	else
		return false;
}
