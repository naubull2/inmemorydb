//
//  LogManager.h
//  DBhw04
//  Wooin Lee
//  2014-22572
//

#ifndef LogManager_h
#define LogManager_h

#include <stdlib.h>
#include <mutex>
#include <string>
#include <time.h>
#include <map>
#include <math.h>

#include "CBuffer.h"

class LogManager{
public:
    LogManager();
    ~LogManager();

	void addLogBuffer(std::string log);
	void runFlushThread();
	void hardFlush();

private:
	FILE* logfile;
	int mLSN;
	int leftOver;
	bool isStop;
	bool flushAll;
	std::mutex log_mutex;

   	// circular queue for holding LSN as buffer
	CBuffer* log_buffer = new CBuffer();
	std::map<int, std::string> log_records;	

	void Flush();
};

#endif /* LogManager_h */
