//
//  ColumnTable.h
//  DBhw03
//  Wooin Lee
//  2014-22572
//

#ifndef ColumnTable_h
#define ColumnTable_h

#include "Util.h"
#include "RowTable.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <iomanip>


#define COL_PAGE_SIZE 1000

using namespace std;


typedef struct ColumnPage{
	char* records;
}ColumnPage;

typedef struct Column{
	int recordSize;									// record size of current column(key + value)
	int numRecord;									// number of records in the last TablePage in the page list
	vector<ColumnPage*> *pages;			// keeps list of page poniters
}Column;

typedef struct ColScheme{
	int field_count;
	int* field_type;
}ColScheme;

typedef unordered_map<int, int> intDictionary;
typedef unordered_map<std::string, int> doubleDictionary;

typedef unordered_map<int, int> intintRehash;
typedef unordered_map<int, double> intdoubleRehash;


class ColumnTable{
public:

	FILE* ofs;
	ColScheme mScheme;
	Column* mColumns;		// Columns

	// Dictionaries of different types
	intDictionary *intDict;
	doubleDictionary *doubleDict;

	intintRehash *intRehash;				// rehash for constant dictionary lookup
	intdoubleRehash *doubleRehash;

	ColumnPage* pageBuffer;

	int n_intDict, n_doubleDict;

	//ColumnTable(FILE* ofs_);																																																			  
	//ColumnTable(Scheme scheme_, FILE* ofs_);                // create table with given scheme
	ColumnTable(RowTable* rowTable);				// create columnar table from a row table
	~ColumnTable();

	void scan(int col, double filter_value);
	double average1(int col);	 // naive average- lookup every value on the dictionary
	double average2(int col);  // count vids then lookup dictionary once per entry

private:

	////////////////

	int getDictId(int col);	// get the dictionary index

	//void buildDictionary(intDictionary* dict, int total_size);
	//void buildDictionary(doubleDictionary* dict, int total_size);
	
	void insertDictionary(intDictionary* dict, intintRehash* rehash, int val);
	void insertDictionary(doubleDictionary* dict, intdoubleRehash* rehash, double val);

	// return index of the dictionary which is then used to create bitset
	int getIndex(intDictionary* dict, int val);
	int getIndex(doubleDictionary* dict, double val);

	int getValue(intintRehash* dict, int idx);
	double getValue(intdoubleRehash* dict, int idx);
																	 
	// look up corresponding dictionary to get compressed key, then insert to the column
	void insertRecordCol(Column* column, int index, int val);

	void printRecord(int page, int idx);
	string to_string_precision(double val);
	
};


#endif /* ColumnTable_h */
