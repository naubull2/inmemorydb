//
//  XactManager.h
//  DBhw04
//  Wooin Lee
//  2014-22572
//
#ifndef XactManager_h
#define XactManager_h

#include <stdlib.h>
#include <mutex>
#include <string>
#include <time.h>
#include <map>

using namespace std;


class XactManager{
public:
    XactManager();
    ~XactManager();
	int getXid();	// issue a new transaction id
	clock_t getInitTime(int xid);	// return init time of xid
	clock_t getCommitTime(int xid); // return commit time of xid
	bool removeXid(int xid);	// commit and remove transaction id from the running list

private:
	int mXid;
	mutex xact_mutex;
	map<int, clock_t> mRunList;	// Xid table, holding <Xid, timestamp(system clock)>
	map<int, clock_t> mCommitList; 	// Xid table, holding commit times of finished tasks
    
};


#endif /* XactManager_h */
