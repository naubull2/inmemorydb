//
//  MVCC.h
//  DBhw05
//  Wooin Lee
//  2014-22572
//

#ifndef MVCC_h
#define MVCC_h

#include <stdlib.h>
#include <stdio.h>
#include <mutex>
#include <string>
#include <time.h>
#include <map>
#include <vector>
#include <math.h>

#include "RowTable.h"
#include "Util.h"

/*
 * Version must contain the timestamp of the "transaction commit time"
 * Until the transaction that created the version is committed, version
 * shouldn't be writtten on to the version space.
 */
typedef struct Version{
	int TS;
	std::string data;
}Version;

class MVCC{
public:
    MVCC();
    ~MVCC();

	void bindTable(RowTable* db_);

	void createVersion(RID rid, std::string data);
	void runGCThread();
	bool checkVersion(RID rid);
	std::string readLatestVersion(RID rid, int time_stamp);
	std::string readLatestVersion(RID rid, int time_stamp, int col);

	void versionPrint(FILE* fp);

private:
	bool isStop;
	RowTable* db;
	std::map<RID, int> GClock;

	std::map<RID, std::vector<Version>> versionSpace; // version hash table
	std::mutex versionSpace_mutex;
};

#endif /* MVCC_h */
