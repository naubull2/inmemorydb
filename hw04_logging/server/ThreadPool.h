//
// ThreadPool.h
// DBhw04
// Wooin Lee
// 2014-22572
//
#ifndef ThreadPool_h
#define ThreadPool_h

#include <thread>
#include <mutex>
#include <vector>
#include <queue>
#include <condition_variable>

class ThreadPool;

class Worker{
	public:
		Worker(ThreadPool &s) : pool(s){ }
		void operator()();
	private:
		ThreadPool &pool;
};

class ThreadPool{
	public:
		ThreadPool(size_t);
		template<class F> void enqueue(F f);
		~ThreadPool();
	private:
		friend class Worker;
		// running thread list
		std::vector<std::thread > workers;
		// task queue
		std::deque<std::function<void()>> tasks;

		// synchronization
		std::mutex queue_mutex;
		std::condition_variable condition;
		bool stop;
};


// Worker implementation
void Worker::operator()(){
	std::function<void()> task;
	while(true){
		{//	CRITICAL section 
			std::unique_lock<std::mutex>
				lock(pool.queue_mutex);

			// look for a work instance
			while(!pool.stop && pool.tasks.empty()){
				pool.condition.wait(lock);
			}

			if(pool.stop)	// exit when the pool is terminated
				return;
			
			// fetch task
			task = pool.tasks.front();
			pool.tasks.pop_front();

		}// exit CRITICAL section

		// execute
		task();
	}
}

ThreadPool::ThreadPool(size_t threads): stop(false){
	for(size_t i = 0; i < threads; i++)
		workers.push_back(std::thread(Worker(*this)));
}
ThreadPool::~ThreadPool(){
	// signal all threads to stop
	stop = true;
	condition.notify_all();
	// join
	for(size_t i = 0; i < workers.size(); i++)
		workers[i].join();
}

template<class F> 
void ThreadPool::enqueue(F f){
	{// enter CRITICAL section
		std::unique_lock<std::mutex> lock(queue_mutex);

		tasks.push_back(std::function<void()>(f));
	}// exit CRITICAL section

	// wake up a free thread
	condition.notify_one();
}
#endif /* ThreadPool_h */
