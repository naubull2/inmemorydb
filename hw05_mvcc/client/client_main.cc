
#include "ClientSocket.h"
#include "SocketException.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

int main(int argc, char* argv[]) {
    try {
        // Create the Socket
        ClientSocket c_socket("localhost", 30000);
        std::string line;
        std::string reply;
		std::string scan_message = "SCAN|100";

        //std::ifstream in("../../data/Homework2-lineitem03456.csv");
        // Small datasets for debugging
		std::ifstream in("lineitem_sample.tbl");
		std::ifstream up("update.tbl");
        //std::ifstream in("../../data/lineitem_small.csv");
        std::ofstream out("client.log");

		int iter = 0;
        while(!in.eof()) {
            getline(in, line);
            if(line != "") {
                try {
                    std::string message = "INSERT|" + line;
                    out << "[Sending]\t" + message + "\n";
                    std::cout << "[Sending]\t" + message + "\n";
                    c_socket << message;
                    c_socket >> reply;
                } catch (SocketException&) {}
                out << "[Response]\t" << reply << "\n";
                std::cout << "[Response]\t" << reply << "\n";
            }
			// debugging update function
			if(iter++ > 100)
				break;
        }
		try{
			out << "[Sending]\t" + scan_message + "\n";
			std::cout << "[Sending]\t" + scan_message + "\n";
			c_socket << scan_message;
			c_socket >> reply;
		} catch (SocketException&) {}
		out << "[Response]\t" << reply << "\n";
		std::cout << "[Response]\t" << reply << "\n";

		///////SNAPSHOT///////////////////////////////////////
		try{
			std::string message = "SNAPSHOT";
			out << "[Sending]\t" + message + "\n";
			std::cout << "[Sending]\t" + message + "\n";
			c_socket << message;
			c_socket >> reply;
		}catch(SocketException&){}
		out << "[Response]\t" << reply << "\n";
		std::cout << "[Response]\t" << reply<< "\n";
		///////////////////////////////////////////////////////
		int linenum = 0;
		int snap = 0;
		while(!up.eof()){
			getline(up, line);
			if(line != ""){
				try{
                    std::string message = "UPDATE|" + std::to_string(linenum) + "|" + line;
                    out << "[Sending]\t" + message + "\n";
                    std::cout << "[Sending]\t" + message + "\n";
                    c_socket << message;
                    c_socket >> reply;
				}catch(SocketException&){}
				out << "[Response]\t" << reply << "\n";
				std::cout << "[Response]\t" << reply<< "\n";
			}
				
			if(snap  % 10 == 0){	
				try{
					out << "[Sending]\t" + scan_message + "\n";
					std::cout << "[Sending]\t" + scan_message + "\n";
					c_socket << scan_message;
					c_socket >> reply;
				} catch (SocketException&) {}
				out << "[Response]\t" << reply << "\n";
				std::cout << "[Response]\t" << reply << "\n";
				///////SNAPSHOT///////////////////////////////////////
				try{
					std::string message = "SNAPSHOT";
					out << "[Sending]\t" + message + "\n";
					std::cout << "[Sending]\t" + message + "\n";
					c_socket << message;
					c_socket >> reply;
				}catch(SocketException&){}
				out << "[Response]\t" << reply << "\n";
				std::cout << "[Response]\t" << reply<< "\n";
				///////////////////////////////////////////////////////
			}
			snap++;
		//	if(linenum > 10)
		//		break;
		}
		///////SNAPSHOT///////////////////////////////////////
		try{
			std::string message = "SNAPSHOT";
			out << "[Sending]\t" + message + "\n";
			std::cout << "[Sending]\t" + message + "\n";
			c_socket << message;
			c_socket >> reply;
		}catch(SocketException&){}
		out << "[Response]\t" << reply << "\n";
		std::cout << "[Response]\t" << reply<< "\n";
		///////////////////////////////////////////////////////
    } catch(SocketException& e) {
        std::cout << "Exception caught: " << e.description() << std::endl;
    }
    return 0;
}
