//
// XactManager.cc
// DBhw04
// Wooin Lee
// 2014-22572
//
#include "XactManager.h"

XactManager::XactManager(){
	mXid = 0;
}

XactManager::~XactManager(){
	mRunList.clear();
}

int XactManager::getXid(){
	unique_lock<mutex> lock(xact_mutex);
	int newXid = mXid++;

	mRunList[newXid] = clock();
	return newXid;
}

bool XactManager::removeXid(int xid){
	unique_lock<mutex> lock(xact_mutex);
	// erase xid from running list and add to commit list
	mRunList.erase(xid);
	mCommitList[xid] = clock();
	return true;
}

clock_t XactManager::getInitTime(int xid){
	std::unique_lock<std::mutex> lock(xact_mutex);
	return mRunList[xid];
}

clock_t XactManager::getCommitTime(int xid){
	std::unique_lock<std::mutex> lock(xact_mutex);
	clock_t commitTime = mCommitList[xid];
	mCommitList.erase(xid);
	return commitTime;
}

